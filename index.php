<?php
	error_reporting(0);
	require_once("MaximaFMScraper.php");
	$scraper = new MaximaFMScraper("http://www.maxima.fm/51Chart/");
	$puestos = $scraper->get_puestos();
	$artistas = $scraper->get_artistas();
	$titulos = $scraper->get_titulos();
	$semanas = $scraper->get_semanas();
	$canciones = $scraper->crear_canciones($puestos, $artistas, $titulos, $semanas);
?>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap -->
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  		<h1>Maxima 51 Chart</h1><p>Color Rojo = Novedad en lista</p>
		<table class="table">
			<thead>
				<tr>
					<td><strong>Puesto</strong></td>
					<td><strong>Artista</strong></td>
					<td><strong>Título</strong></td>
					<td><strong>Youtube</strong></td>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($canciones as $cancion) {?>
					<?php echo $scraper->pintar_canciones($cancion['num_semanas']) ?>
						<td><?php echo $cancion['Puesto']?></td>
						<td><?php echo $cancion['Artista']?></td>
						<td><?php echo $cancion['Titulo']?></td>
						<td><a href="<?php echo $cancion['Url']?>" target="_blank">Link</a></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</body>
</html>