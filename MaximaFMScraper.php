<?php
	class MaximaFMScraper{
		private $html;
		private $scraper;
		private $scraper_xpath;
		
		public function __construct($url){
			$this->html = file_get_contents($url);
			$this->scraper = new DOMDocument();
			$this->scraper->loadHTML(utf8_encode($this->html));
			libxml_clear_errors(); //remove errors for yucky html
			error_reporting(E_ERROR | E_PARSE);
			$this->scraper_xpath = new DOMXPath($this->scraper);
		}
		public function get_puestos(){
			//Escribimos los puestos desde el 1 al 51
			$puestos = array();
			for ($i=1; $i < 52; $i++) { 
				array_push($puestos, $i);
			}
			return $puestos;
		}
		public function get_artistas(){
			//Seleccionamos los span que tengan la clase normal
			$artistas_lista = $this->scraper_xpath->query('//span[@class="normal"]');
			//Almacenamos las artistas en un array
			if($artistas_lista->length > 0){
				$artistas = array();
		      	foreach($artistas_lista as $artista){
		      		array_push($artistas, $artista->nodeValue);
		      	}
		  	}
			return $artistas;
		}
		public function get_titulos(){
			//Seleccionamos los span que tengan la clase grande para obtener el titulo del puesto 1
			$titulo_top1 = $this->scraper_xpath->query('//span[@class="grande"]');
			$titulo_1 = $titulo_top1->item(0)->nodeValue;
			//Seleccionamos los span que tengan la clase negrita para obtener el resto de titulos
			$titulos_lista = $this->scraper_xpath->query('//span[@class="negrita"]');
			//Almacenamos los titulos en un array
			if($titulos_lista->length > 0){
				$titulos = array();
				array_push($titulos, $titulo_1);
		      	foreach($titulos_lista as $titulo){
		      		array_push($titulos, $titulo->nodeValue);
		      	}
		  	}
			return $titulos;
		}
		
		public function get_semanas(){
			//Seleccionamos los td que tengan la clase verde para obtener las semanas en lista de la canción del puesto 1
			$semanas_lista_top = $this->scraper_xpath->query('//td[@class="verde"]');
			//Seleccionamos los td que tengan la clase ab para obtener las semanas en lista de la canción de último puesto
			$semanas_lista_ultimo = $this->scraper_xpath->query('//td[@class="ab"]');
			//Seleccionamos los td que tengan la clase puesto como referencia para obtener posteriormente el número de semanas en lista de la canción
			$semanas_lista = $this->scraper_xpath->query('//td[@class="puesto"]');
			
			$num_1 = $semanas_lista_top->item(0)->nodeValue;
			$ultimo = $semanas_lista_ultimo->item(0)->nodeValue;
			//Almacenamos los ml en un array
			if($semanas_lista->length > 0){
				$semanas = array();
				array_push($semanas, $num_1);
		      	foreach($semanas_lista as $semana){
		      		//Guardamos en el array el cuarto hijo (sem. lista) del padre del puesto de la canción
				  	array_push($semanas, $semana->parentNode->childNodes->item(4)->nodeValue);
		      	}
				array_push($semanas, $ultimo);
		  	}
			return $semanas;
		}
		
		public function buscar_youtube($artista, $titulo){
			//Evitamos que nos de un error de maximum_execution_time exceded
			ini_set('max_execution_time', 0);
			//Creamos la ruta de búsqueda en youtube para canción
			$url= "https://www.youtube.com/results?search_query=";
			$cadena = $artista."&".$titulo;
			$cadena = str_replace(" ","+",$cadena);
			$url = $url.$cadena;
			//Creamos un nuevo scraper, esta vez para la página de búsqueda de youtube
			$this->__construct($url);
			//El enlace de cada vídeo se guarada en una etiqueta a con la clase contains-addto yt-uix-sessionlink     spf-link 
			$videos = $this->scraper_xpath->query('//a[@class="contains-addto yt-uix-sessionlink     spf-link "]');
			foreach ($videos as $video) {
				//Una vez encontrado el primer vídeo de los resultados de búsqueda, salimos del bucle ya que el vídeo es válido
				$url = "https://www.youtube.com".$video->getAttribute("href");
				break;
			}
			return $url;
		}
		
		public function crear_canciones($puestos, $artistas, $titulos, $semanas){
			//Evitamos que nos de un error de maximum_execution_time exceded
			ini_set('max_execution_time', 0);
			//Creamos un array de canciones con el puesto, el artista, el titulo y el número de semanas en lista de cada una
			$canciones = array();
			for ($i=0; $i < sizeof($artistas); $i++) { 
				$canciones[$i]['Puesto'] = $puestos[$i];
				$canciones[$i]['Artista'] = $artistas[$i];
				$canciones[$i]['Titulo'] = $titulos[$i];
				$canciones[$i]['Url'] = $this->buscar_youtube($artistas[$i], $titulos[$i]);
				$canciones[$i]['num_semanas'] = $semanas[$i];
			}
			return $canciones;
		}
		
		//Pinta la fila de la tabla de rojo si la canción es una novedad en lista
		public function pintar_canciones($num_semanas){
			if ((int) $num_semanas == 1) {
				echo ("<tr style='background-color: #FF8080'>");	
			} else {
				echo ("<tr>");
			}
		}
	}
?>